import React, {useState} from "react";
import { Text, View, StyleSheet, Image, TouchableOpacity, Alert, Platform } from "react-native";
//import image from './assets/diamante.png'
import * as ImagePicker from "expo-image-picker";
import * as Sharing from "expo-sharing";
import uploadToAnonymousFilesAsync  from "anonymous-files";
const App = () => {
  const [selectedImage, setSelectedImage] = useState(null)

  let openImagePickerAsync = async() =>{
   let permissionResult = await ImagePicker.requestCameraPermissionsAsync();

   if(permissionResult.granted === false){
    alert("Los permisos para acceder a la galeria son requeridos");
    return;
   }
   const pickerResult = await ImagePicker.launchImageLibraryAsync()
   if(pickerResult.cancelled === true){
    return;
   }

    if(Platform.OS === "web"){
      let remoteUri = await uploadToAnonymousFilesAsync(pickerResult.uri);
      setSelectedImage({localUri: pickerResult.uri, remoteUri});
    }else{
      setSelectedImage({localUri: pickerResult.uri});
    }
  };

  let openShareDialog = async() => {
    if(!(await Sharing.isAvailableAsync())){
      alert(`La imagen esta disponible para compartir:${selectedImage.remoteUri}`);
      return;
    }

    await Sharing.shareAsync(selectedImage.localUri);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}> Selecciona una imagen</Text>
      <TouchableOpacity onPress={openImagePickerAsync}>
      <Image 
       source={{uri: selectedImage !== null ? selectedImage.localUri : "https://picsum.photos/180/180",}} 
      //source={image}
       style={styles.image}
       />
      </TouchableOpacity>
      {
        selectedImage ? (
      <TouchableOpacity onPress={ openShareDialog } style={styles.buttom}>
        <Text style={styles.buttomText}>Share</Text>
      </TouchableOpacity>
      ) : (
         <View></View>
      )}
 </View>
  );
};
const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: "#292929" },
  title: { fontSize: 30, color: "#f0f8ff" },
  image: {height:200, width: 200, borderRadius: 100, resizeMode: 'contain'},
  buttom: {backgroundColor: "#C0BBBE", padding: 7, marginTop: 10, borderRadius: 15},
  buttomText: {color: "#fff", fontSize: 20}
})

export default App;
